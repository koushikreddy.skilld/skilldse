import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';

class AddInformationPage extends StatefulWidget {
  @override
  _AddPersonPageState createState() => _AddPersonPageState();
}

class _AddPersonPageState extends State<AddInformationPage> {
  
  final DatabaseReference database =
      FirebaseDatabase.instance.reference().child('test');

  List<String> lists = new List();
  var value;
  var name;
  var number;
  var address;
  var foodItem;
  var numberOfPerson;
  var amountPerDay;
  String nameString;
  final TextEditingController nameController = new TextEditingController();
  final TextEditingController numberController = new TextEditingController();
  final TextEditingController addressControler = new TextEditingController();
  final TextEditingController foodItemControler = new TextEditingController();
  final TextEditingController numberOfPersonControler =
      new TextEditingController();
  final TextEditingController amountPerDayControler =
      new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Add Info'),
        ),
        body: SingleChildScrollView(
                  child: Column(
            children: <Widget>[
              Row(children: <Widget>[
                Text('Name  '),
                Container(
                  width: 300,
                  child: TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      hintText: 'Name',
                    ),
                  ),
                )
              ]),
              Row(
                children: <Widget>[
                  Text('Number'),
                  Container(
                      width: 300,
                      child: TextField(
                        controller: numberController,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), hintText: 'number'),
                      ))
                ],
              ),
              Row(
                children: <Widget>[
                  Text('Address'),
                  Container(
                      width: 300,
                      child: TextField(
                        controller: addressControler,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), hintText: 'address'),
                      ))
                ],
              ),
              Row(
                children: <Widget>[
                  Text('FoodItem'),
                  Container(
                      width: 300,
                      child: TextField(
                        controller: foodItemControler,
                        decoration: InputDecoration(
                            border: OutlineInputBorder(), hintText: 'FoodItem'),
                      ))
                ],
              ),
              Row(
                children: <Widget>[
                  Text('NumberOfPerson'),
                  Container(
                      width: 300,
                      child: TextField(
                        controller: numberOfPersonControler,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(border: OutlineInputBorder()),
                      ))
                ],
              ),
              Row(
                children: <Widget>[
                  Text('AmountPerDay'),
                  Container(
                      width: 300,
                      child: TextField(
                        controller: amountPerDayControler,
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(border: OutlineInputBorder()),
                      ))
                ],
              ),
              RaisedButton(
                onPressed: () {
                  setState(() {
                    name = nameController.text;
                    number = numberController.text;
                    address = addressControler.text;
                    foodItem = foodItemControler.text;
                    numberOfPerson = numberOfPersonControler.text;
                    amountPerDay = amountPerDayControler.text;
                    

                    nameController.clear();
                    numberController.clear();
                    addressControler.clear();
                    foodItemControler.clear();
                    numberOfPersonControler.clear();
                    amountPerDayControler.clear();


                    database.push().set({
                      'name': name,
                      'number': number,
                      'address': address,
                      'foodItem': foodItem,
                      'numberOfPerson': numberOfPerson,
                      'amountPerDay': amountPerDay
                      
                    });
                    print(name);

                  });
                },
                child: Text('save'),
              ),
              Row(
                children: <Widget>[
                  new StreamBuilder<Event>(
                    stream: FirebaseDatabase.instance
                        .reference()
                        .child('test')
                        //.child('-M5kOX6YXQkKlrWUR4PP')
                        .onValue,
                    builder: (BuildContext context, AsyncSnapshot<Event> event) {
                      if (!event.hasData)
                      print(event.hasData);
                        return new Center(child: new Text('Loading...'));
                      lists.clear();
                      Map<dynamic, dynamic> data = event.data.snapshot.value;
                      print(data);
                      data.forEach((key, values) {
                        lists.add(values); // print(key + ': ' + value);
                      });
                      print(lists.length);
                      return Column(children: [
                        Text('${data['name']}'),
                        Text('${data['number']}')
                      ]);
                    },
                  ),
                ],
              ),
              // Row(
              //   children: <Widget>[
              //     FutureBuilder(
              //         future: database.once(),
              //         builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              //           if (snapshot.hasData) {
              //             lists.clear();
              //             Map<dynamic, dynamic> values = snapshot.data.value;
              //             values.forEach((key, values) {
              //               lists.add(values);
              //             });
              //             return new ListView.builder(
              //                 shrinkWrap: true,
              //                 itemCount: lists.length,
              //                 itemBuilder: (BuildContext context, int index) {
              //                   return Card(
              //                     child: Column(
              //                       crossAxisAlignment: CrossAxisAlignment.start,
              //                       children: <Widget>[
              //                         Text("Name: " + lists[index]["name"]),
              //                         Text("Age: " + lists[index]["age"]),
              //                         Text("Type: " + lists[index]["type"]),
              //                       ],
              //                     ),
              //                   );
              //                 });
              //           }
              //           return CircularProgressIndicator();
              //         })
              //   ],
              // ),
              // Row(
              //   children: <Widget>[
              //     FutureBuilder(
              //         future: database.once(),
              //         builder: (context, AsyncSnapshot<DataSnapshot> snapshot) {
              //           if (snapshot.hasData) {
              //             lists.clear();
              //             Map<dynamic, dynamic> values = snapshot.data.value;
              //             values.forEach((key, values) {
              //               lists.add(values);
              //             });

              //             new Expanded(
              //                 child: new ListView.builder(
              //                     itemCount: lists.length,
              //                     itemBuilder: (BuildContext ctxt, int Index) {
              //                       return new Text(lists[Index]);
              //                     }));
              //           }
              //           return CircularProgressIndicator();
              //         })
              //   ],
              // )
            ]
          ),
        ));
  }
}
